###############################################################
# SIEMonster Alerts App
# Author: Ilia A. (ilia@siemonster.com, mail@harduino.com)
###############################################################
import socket
import requests

from walkoff_app_sdk.app_base import AppBase
from siemonster_core import initSIEMonster, SIEMonsterHelpers

class SiemonsterEventSearch(AppBase):
    __version__ = "1.0.0"
    app_name = "SIEMonster Alerts"  # this needs to match "name" in api.yaml

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    # form required headers to interact with Alerta API
    def get_headers(self):
        headers=self.siemonster_auth.get_auth_headers()
        return {
            **headers,
            'Content-Type': 'application/json',
            'Accept': 'application/json;charset=utf-8',
        }

    # get events with using filters
    def get_alerts(
            self,
            url,
            key,
            filter_text,
            filter_severity,
            filter_group,
            page,
            page_size,
            sort_by,
        ):
        initSIEMonster(self, url, key)

        params = {}
        self.logger.info(f"filter_text: {filter_text}")
        if filter_text:
            params['q'] = f"event:\"{filter_text}\""

        self.logger.info(f"filter_severity: {filter_severity}")
        if filter_severity and filter_severity != "- any -":
            params['severity'] = filter_severity

        self.logger.info(f"filter_group: {filter_group}")
        if filter_group:
            params['group'] = filter_group

        self.logger.info(f"page: {page}")
        if page:
            params['page'] = page

        self.logger.info(f"page_size: {page_size}")
        if page_size:
            params['page-size'] = page_size

        self.logger.info(f"sort_by: {sort_by}")
        if sort_by and sort_by != "- default -":
            params['sort-by'] = sort_by
        
        self.logger.info(f"PARAMS: {params}")

        url = f"{self.siemonster_api.get_events_url()}/alerts"
        ret = requests.get(
            url,
            verify=False,
            params=params,
            headers=self.get_headers(),
        )
        ret.raise_for_status()
        return ret.json()

    # send specified event to TheHive
    def send_to_ir(
            self,
            url,
            key,
            event_id,
        ):
        initSIEMonster(self, url, key)

        url = f"{self.siemonster_api.get_events_url()}/alert/{event_id}/send-to-hive"
        ret = requests.post(
            url,
            verify=False,
            data={},
            headers=self.get_headers(),
        )
        ret.raise_for_status()
        return ret.json()

    # toggle event auto-sending to TheHive
    def set_auto_send(
            self,
            url,
            key,
            event_id,
            auto_send,
        ):
        initSIEMonster(self, url, key)

        auto_send = True if auto_send == "true" else False
        data = {"autoSend": auto_send}
        self.logger.info(f"data: {data}")

        url = f"{self.siemonster_api.get_events_url()}/alert/{event_id}/set-auto-send"
        self.logger.info(url)
        ret = requests.post(
            url,
            verify=False,
            json=data,
            headers=self.get_headers(),
        )
        ret.raise_for_status()
        return ret.json()

    # delete specified Alerta events
    def delete_events(
            self,
            url,
            key,
            event_ids,
        ):
        initSIEMonster(self, url, key)

        url = f"{self.siemonster_api.get_events_url()}/alerts"
        ret = requests.delete(
            url,
            verify=False,
            json={
                "ids": SIEMonsterHelpers.listFromString(event_ids)
            },
            headers=self.get_headers(),
        )
        ret.raise_for_status()
        return ret.json()

if __name__ == "__main__":
    SiemonsterEventSearch.run()
