###############################################################
# SIEMonster Event Search App
# Author: Ilia A. (ilia@siemonster.com, mail@harduino.com)
###############################################################
import socket
import requests

from walkoff_app_sdk.app_base import AppBase
from siemonster_core import initSIEMonster

class SiemonsterEventSearch(AppBase):
    __version__ = "1.0.0"
    app_name = "SIEMonster Event Search"  # this needs to match "name" in api.yaml

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def get_cluster_health(self, url, key):
        initSIEMonster(self, url, key)
        es_url = self.siemonster_svc.get_es_url()

        return requests.get(es_url + "/_cluster/health", verify=False).json()

    def execute_query(self, url, key, method, path, body):        
        initSIEMonster(self, url, key)
        es_url = self.siemonster_svc.get_es_url()

        headers = {
            "Accept": "application/json",
            "Content-type": "application/json",
        }

        return requests.request(
            method,
            es_url + path,
            data=body,
            headers=headers,
            verify=False
        ).text

if __name__ == "__main__":
    SiemonsterEventSearch.run()
