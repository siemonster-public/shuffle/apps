###############################################################
# SIEMonster Incident Response App v1.1.3
# Authors:
# - @frikkylikeme (https://github.com/frikky)
# - Ilia A. (ilia@siemonster.com, mail@harduino.com)
###############################################################
import time
import json
import requests
import thehive4py
from requests import RequestException

from thehive4py.api import TheHiveApi
from thehive4py.query import *
import thehive4py.models
from thehive4py.models import *

from walkoff_app_sdk.app_base import AppBase
from siemonster_core import initSIEMonster, SIEMonsterHelpers

class SiemonsterIncidentResponse(AppBase):
    __version__ = "1.0.0"
    app_name = "SIEMonster Incident Response"  # this needs to match "name" in api.yaml

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def __connect_thehive(self, url, key, version=3):
        url = SIEMonsterHelpers.unslashifyEnd(url)
        initSIEMonster(self, url, key)

        url = f"{url}/g/v2/svc/{self.siemonster_svc.get_current_tenant_id()}/incident-response"
        self.thehive = TheHiveApi(
            url,
            self.siemonster_auth.generate_auth_token(
                self.siemonster_svc.get_payload_key_to_force_session('incident-response')
            ),
            cert=False,
            version=version,
        )

    def search_case_title(self, url, key, title_query):
        self.__connect_thehive(url, key)

        try:
            ret = self.thehive.find_cases(
                query=ContainsString("title", title_query), range="all", sort=[]
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def custom_search(
        self, url, key, search_for, custom_query, range="all"
    ):
        self.__connect_thehive(url, key)

        try:
            if not isinstance(custom_query, list) and not isinstance(custom_query, dict) and not isinstance(custom_query, object):
                try:
                    custom_query = json.loads(custom_query)
                except:
                    # raise IOError("Invalid JSON payload received.")
                    pass

            if search_for == "alert":
                ret = self.thehive.find_alerts(
                    query=custom_query, range="all", sort=[]
                )
            else:
                ret = self.thehive.find_cases(query=custom_query, range="all", sort=[])
            ret.raise_for_status()

            if (
                ret.status_code == 200
                or ret.status_code == 201
                or ret.status_code == 202
            ):
                return ret.text
            else:
                raise IOError(ret.text)
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)
        

    def add_case_artifact(
        self,
        url,
        key,
        case_id,
        data,
        datatype,
        tags=None,
        tlp=None,
        ioc=None,
        sighted=None,
        description="",
    ):
        self.__connect_thehive(url, key)

        tlp = int(tlp) if tlp else 2
        ioc = True if ioc.lower() == "true" else False
        sighted = True if sighted.lower() == "true" else False
        if not description:
            description = "Created by shuffle"

        tags = (
            tags.split(", ") if ", " in tags else tags.split(",") if "," in tags else []
        )

        item = thehive4py.models.CaseObservable(
            dataType=datatype,
            data=data,
            tlp=tlp,
            ioc=ioc,
            sighted=sighted,
            tags=tags,
            message=description,
        )
    
        try:
            ret = self.thehive.create_case_observable(case_id, item)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def search_alert_title(
        self, url, key, title_query, search_range="0-25"
    ):
        self.__connect_thehive(url, key)

        # Could be "all" too
        if search_range == "":
            search_range = "0-25"

        try:
            ret = self.thehive.find_alerts(
                query=ContainsString("title", title_query), range=search_range, sort=[]
            ) 
            ret.raise_for_status()          
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def create_case(
        self,
        url,
        key,
        template,
        title,
        description="",
        tlp=None,
        pap=None,
        severity=None,
        flag=None,
        tags="",
        custom_fields=None,
        custom_json=None,
    ):
        self.__connect_thehive(url, key)

        flag = False if flag.lower() == "false" else True
        pap = int(pap) if pap else 2
        tlp = int(tlp) if tlp else 2
        severity = int(severity) if severity else 2
        tags = tags.split(",") if tags else []

        if tlp > 3 or tlp < 0:
            return f"TLP needs to be a number from 0-3, not {tlp}"
        if severity > 4 or severity < 1:
            return f"Severity needs to be a number from 1-4, not {severity}"

        Casetemplate = template if template else None

        # Prepare the customfields
        customfields = CustomFieldHelper()

        if isinstance(custom_fields, str):
            try:
                custom_fields = json.loads(custom_fields) if custom_fields else {}
            except json.decoder.JSONDecodeError:
                return "Custom fields need to be valid json"

        for key, value in custom_fields.items():
            if type(value) == int:
                customfields.add_integer(key, value)
            elif type(value) == str:
                customfields.add_string(key, value)
            elif type(value) == bool:
                customfields.add_boolean(key, value)
            elif type(value) == float:
                customfields.add_float(key, value)
            else:
                print(
                    f'The value type "{value}" of the field {key} is not suported by the function.'
                )


        # Fields in JSON
        customfields = customfields.build()
        custom_json = json.loads(custom_json) if custom_json else {}

        case = thehive4py.models.Case(
            title=title,
            tlp=tlp,
            pap=pap,
            severity=severity,
            flag=flag,
            tags=tags,
            description=description,
            template=Casetemplate,
            customFields=customfields,
            json=custom_json,
        )

        try:
            ret = self.thehive.create_case(case)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def create_alert(
        self,
        url,
        key,
        type,
        source,
        sourceref,
        title,
        description="",
        tlp=1,
        severity=1,
        tags="",
        artifacts="",
    ):
        self.__connect_thehive(url, key)
        if tags:
            if ", " in tags:
                tags = tags.split(", ")
            elif "," in tags:
                tags = tags.split(",")
            else:
                tags = [tags]
        else:
            tags = []

        # Wutface fix
        if not tlp:
            tlp = 1
        if not severity:
            severity = 1

        if isinstance(tlp, str):
            if not tlp.isdigit():
                return "TLP needs to be a number from 0-3, not %s" % tlp

            tlp = int(tlp)
        if isinstance(severity, str):
            if not severity.isdigit():
                return "Severity needs to be a number from 1-4, not %s" % severity

            severity = int(severity)

        if tlp > 3 or tlp < 0:
            return "TLP needs to be a number from 0-3, not %d" % tlp
        if severity > 4 or severity < 1:
            return f"Severity needs to be a number from 1-4, not {severity}"

        all_artifacts = []
        if isinstance(artifacts, str) and artifacts != '':
            # print("ITS A STRING!")
            try:
                artifacts = json.loads(artifacts)
            except:
                print("[ERROR] Error in parsing artifacts!")

        # print("ART HERE: %s" % artifacts)
        # print("ART: %s" % type(artifacts))
        if isinstance(artifacts, list):
            print("ITS A LIST!")
            for item in artifacts:
                print("ITEM: %s" % item)
                try:
                    artifact = thehive4py.models.AlertArtifact(
                        dataType=item["data_type"],
                        data=item["data"],
                    )

                    try:
                        artifact["message"] = item["message"]
                    except:
                        pass

                    if item["data_type"] == "ip":
                        try:
                            if item["is_private_ip"]:
                                message += " IP is private."
                        except:
                            pass

                    all_artifacts.append(artifact)
                except KeyError as e:
                    print("Error in artifacts: %s" % e)

        alert = thehive4py.models.Alert(
            title=title,
            tlp=tlp,
            severity=severity,
            tags=tags,
            description=description,
            type=type,
            source=source,
            sourceRef=sourceref,
            artifacts=all_artifacts,
        )

        try:
            ret = self.thehive.create_alert(alert)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def add_alert_artifact(
        self,
        url,
        key,
        alert_id,
        dataType,
        data,
        message=None,
        tlp="2",
        ioc="False",
        sighted="False",
        ignoreSimilarity="False",
        tags=None,
    ):
        self.__connect_thehive(url, key, version=4)

        if tlp:
            tlp = int(tlp)
        else:
            tlp = 2

        ioc = ioc.lower().strip() == "true"
        sighted = sighted.lower().strip() == "true"
        ignoreSimilarity = ignoreSimilarity.lower().strip() == "true"

        if tags:
            tags = [x.strip() for x in tags.split(",")]
        else:
            tags = []

        alert_artifact = thehive4py.models.AlertArtifact(
            dataType=dataType,
            data=data,
            message=message,
            tlp=tlp,
            ioc=ioc,
            sighted=sighted,
            ignoreSimilarity=ignoreSimilarity,
            tags=tags,
        )

        try:
            ret = self.thehive.create_alert_artifact(alert_id, alert_artifact)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Gets an item based on input. E.g. field_type = Alert
    def get_item(self, url, key, field_type, cur_id):
        self.__connect_thehive(url, key)

        try:
            ret = ""
            if field_type.lower() == "alert":
                ret = self.thehive.get_alert(cur_id + "?similarity=1")
            elif field_type.lower() == "case":
                ret = self.thehive.get_case(cur_id)
            elif field_type.lower() == "case_observables":
                ret = self.thehive.get_case_observables(cur_id)
            elif field_type.lower() == "case_task":
                ret = self.thehive.get_case_task(cur_id)
            elif field_type.lower() == "case_tasks":
                ret = self.thehive.get_case_tasks(cur_id)
            elif field_type.lower() == "case_template":
                ret = self.thehive.get_case_tasks(cur_id)
            elif field_type.lower() == "linked_cases":
                ret = self.thehive.get_linked_cases(cur_id)
            elif field_type.lower() == "task_log":
                ret = self.thehive.get_task_log(cur_id)
            elif field_type.lower() == "task_logs":
                ret = self.thehive.get_task_logs(cur_id)
            else:
                raise Exception("%s type is not implemented" % field_type)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def close_alert(self, url, key, alert_id):
        self.__connect_thehive(url, key)
        try:
            ret = self.thehive.mark_alert_as_read(alert_id)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def reopen_alert(self, url, key, alert_id):
        self.__connect_thehive(url, key)
        try:
            ret = self.thehive.mark_alert_as_unread(alert_id)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def follow_alert(self, url, key, alert_id):
        self.__connect_thehive(url, key)
        try:
            ret = self.thehive.follow_alert(alert_id)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def unfollow_alert(self, url, key, alert_id):
        self.__connect_thehive(url, key)
        try:
            ret = self.thehive.unfollow_alert(alert_id)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def create_case_from_alert(
        self, url, key, alert_id, case_template=None
    ):
        self.__connect_thehive(url, key)
        try:
            ret = self.thehive.promote_alert_to_case(
                alert_id=alert_id, case_template=case_template
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def merge_alert_into_case(self, url, key, alert_id, case_id):
        self.__connect_thehive(url, key)
        
        try:
            req = self.thehive.url + f"/api/alert/{alert_id}/merge/{case_id}"
            ret = requests.post(req, auth=self.thehive.auth, headers={'Content-Type': 'application/json'}, verify=False)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Not sure what the data should be
    def update_field(
        self, url, key, field_type, cur_id, field, value_type, data, 
    ):
        self.__connect_thehive(url, key)
        url = self.thehive.url

        # This is kinda silly but..
        try:
            if field_type.lower() == "alert":
                newdata = {}

                if data.startswith("%s"):
                    ticket = self.thehive.get_alert(cur_id)
                    if ticket.status_code != 200:
                        pass

                    newdata[field] = "%s%s" % (ticket.json()[field], data[2:])
                else:
                    # cast data type
                    if value_type == 'int':
                        data = int(data)
                    newdata[field] = data

                # Bleh
                url = "%s/api/alert/%s" % (url, cur_id)
                if field == "status":
                    if data == "New" or data == "Updated":
                        url = "%s/markAsUnread" % url
                    elif data == "Ignored":
                        url = "%s/markAsRead" % url

                    ret = requests.post(
                        url,
                        headers={
                            "Content-Type": "application/json",
                        },
                        auth=self.thehive.auth,
                        verify=False,
                    )
                else:
                    ret = requests.patch(
                        url,
                        headers={
                            "Content-Type": "application/json",
                        },
                        auth=self.thehive.auth,
                        json=newdata,
                        verify=False,
                    )

                ret.raise_for_status()
                return ret.text

            elif field_type.lower() == 'case':
                return 'Use update_case action for updating a case.'
            else:
                return (
                    "%s is not implemented. See https://github.com/frikky/walkoff-integrations for more info."
                    % field_type
                )
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def delete_alert_artifact(self, url, key, artifact_id):
        self.__connect_thehive(url, key, version=4)

        try:
            ret = self.thehive.delete_alert_artifact(artifact_id)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # https://github.com/TheHive-Project/TheHiveDocs/tree/master/api/connectors/cortex
    def run_analyzer(
        self, url, key, cortex_id, analyzer_id, artifact_id
    ):
        self.__connect_thehive(url, key)

        try:
            ret = self.thehive.run_analyzer(cortex_id, artifact_id, analyzer_id)
            ret.raise_for_status()
            return ret.text            
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)


    # Creates a task log in TheHive with file
    def add_task_log(
        self,
        url,
        key,
        task_id,
        message,
        filedata={},
        mime_type=None,
    ):
        self.__connect_thehive(url, key)

        try:
            if filedata["success"] == False:
                return "No file to upload. Skipping message."
        except Exception as e:
            print(f"[WARNING] Error in filedata handler for {filedata}: {e}")

        files = {}
        try:
            if len(filedata["data"]) > 0:
                files = {
                    "attachment": (filedata["filename"], filedata["data"], mime_type),
                }
        except Exception as e:
            print(f"[WARNING] Error in file handler for {filedata} (2): {e}")

        data = {"message": f"{message}"}
        try:
            ret = requests.post(
                f"{self.thehive.url}/api/case/task/{task_id}/log",
                auth=self.thehive.auth,
                files=files,
                data=data,
                verify=False,
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Creates an artifact as a file in a case
    def create_case_file_artifact(
        self, url, key, case_id, tags, filedata
    ):
        self.__connect_thehive(url, key)

        if filedata["success"] == False:
            return "No file to upload. Skipping message."

        if tags:
            if ", " in tags:
                tags = tags.split(", ")
            elif "," in tags:
                tags = tags.split(",")
            else:
                tags = [tags]

        files = {}
        if len(filedata["data"]) > 0:
            files = {
                "attachment": (filedata["filename"], filedata["data"]),
            }

        outerarray = {"dataType": "file", "tags": tags}
        data = {"_json": """%s""" % json.dumps(outerarray)}
        try:
            ret = requests.post(
                "%s/api/case/%s/artifact" % (self.thehive.url, case_id),
                auth=self.thehive.auth,
                files=files,
                data=data,
                verify=False,
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)
        
    # Create an observable as a file for an alert
    def create_alert_file_observable(
        self, url, key, alert_id, tags, filedata
    ):
        self.__connect_thehive(url, key)

        if filedata["success"] == False:
            return "No file to upload. Skipping message."

        if tags:
            if ", " in tags:
                tags = tags.split(", ")
            elif "," in tags:
                tags = tags.split(",")
            else:
                tags = [tags]

        files = {}
        if len(filedata["data"]) > 0:
            files = {
                "attachment": (filedata["filename"], filedata["data"]),
            }

        outerarray = {"dataType": "file", "tags": tags}
        data = {"_json": """%s""" % json.dumps(outerarray)}
        try:
            ret = requests.post(
                "%s/api/alert/%s/artifact" % (self.thehive.url, alert_id),
                auth=self.thehive.auth,
                files=files,
                data=data,
                verify=False,
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)
        

    # Get all artifacts of a given case
    def get_case_artifacts(
        self,
        url,
        key,
        case_id,
        dataType,
    ):
        self.__connect_thehive(url, key)

        query = And(Eq("dataType", dataType)) if dataType else {}

        try:
            ret = self.thehive.get_case_observables(
                case_id, query=query, sort=["-startDate", "+ioc"], range="all"
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    def close_case(
        self,
        url,
        key,
        id,
        resolution_status="",
        impact_status="",
        summary="",
    ):

        self.__connect_thehive(url, key)
        case = self.thehive.case(id)
        case.status = "Resolved"
        case.summary = summary
        case.resolutionStatus = resolution_status
        case.impactStatus = impact_status

        try:
            ret = self.thehive.update_case(
                case,
                fields=[
                    "status",
                    "summary",
                    "resolutionStatus",
                    "impactStatus",
                ],
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Update TheHive Case
    def update_case(
        self,
        url,
        key,
        id,
        title="",
        description="",
        severity=None,
        owner="",
        flag=None,
        tlp=None,
        pap=None,
        tags="",
        status="",
        custom_fields=None,
        custom_json=None,
    ):
        self.__connect_thehive(url, key)

        # Get current case data and update fields if new data exists
        case = self.thehive.get_case(id).json()

        case_title = title if title else case["title"]
        case_description = description if description else case["description"]
        case_severity = int(severity) if severity else case["severity"]
        case_owner = owner if owner else case["owner"]
        case_flag = (
            (False if flag.lower() == "false" else True) if flag else case["flag"]
        )
        case_tlp = int(tlp) if tlp else case["tlp"]
        case_pap = int(pap) if pap else case["pap"]
        case_tags = tags.split(",") if tags else case["tags"]

        case_status = status if status else case["status"]
        case_customFields = case["customFields"]

        # Prepare the customfields
        customfields = CustomFieldHelper()
        if case_customFields:
            for key, value in case_customFields.items():
                if list(value)[0] == "integer":
                    customfields.add_integer(key, list(value.items())[0][1])
                elif list(value)[0] == "string":
                    customfields.add_string(key, list(value.items())[0][1])
                elif list(value)[0] == "boolean":
                    customfields.add_boolean(key, list(value.items())[0][1])
                elif list(value)[0] == "float":
                    customfields.add_float(key, list(value.items())[0][1])
                else:
                    print(
                        f'The value type "{value}" of the field {key} is not suported by the function.'
                    )

        if isinstance(custom_fields, str):
            try:
                custom_fields = json.loads(custom_fields) if custom_fields else {}
            except json.decoder.JSONDecodeError:
                return "Custom fields need to be valid json"

        for key, value in custom_fields.items():
            if type(value) == int:
                customfields.add_integer(key, value)
            elif type(value) == str:
                customfields.add_string(key, value)
            elif type(value) == bool:
                customfields.add_boolean(key, value)
            elif type(value) == float:
                customfields.add_float(key, value)
            else:
                print(
                    f'The value type "{value}" of the field {key} is not suported by the function.'
                )

        customfields = customfields.build()

        custom_json = json.loads(custom_json) if custom_json else {}

        # Prepare the fields to be updated
        case = Case(
            id=id,
            title=case_title,
            description=case_description,
            severity=case_severity,
            owner=case_owner,
            flag=case_flag,
            tlp=case_tlp,
            pap=case_pap,
            tags=case_tags,
            status=case_status,
            customFields=customfields,
            json=custom_json,
        )

        # resolutionStatus=case_resolutionStatus,
        try:
            ret = self.thehive.update_case(
                case,
                fields=[
                    "title",
                    "description",
                    "severity",
                    "owner",
                    "flag",
                    "tlp",
                    "pap",
                    "tags",
                    "customFields",
                    "status",
                ],
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Update TheHIVE alert
    def update_alert(
        self,
        url,
        key,
        id,
        alerttype="",
        source="",
        sourceRef="",
        title="",
        description="",
        tlp=None,
        pap=None,
        severity=None,
        tags="",
        custom_fields=None
    ):
        self.__connect_thehive(url, key)

        # get the current data for the alert
        alert = self.thehive.get_alert(id).json()

        # Update information if given by the user, otherwise, 
        # use the already present data retrieved above
        alert_type = alerttype if alerttype else alert["type"]
        alert_source = source if source else alert["source"]
        alert_sourceRef = sourceRef if sourceRef else alert["sourceRef"]
        alert_title = title if title else alert["title"]
        alert_description = description if description else alert["description"]

        # tlp handling
        alert_tlp = int(tlp) if tlp else alert["tlp"]
            
        if alert_tlp > 3 or alert_tlp < 0:
            return "TLP needs to be a number from 0-3, not %d" % alert_tlp

        # pap handling
        alert_pap = int(pap) if pap else alert["pap"]

        if alert_pap > 3 or alert_pap < 0:
            return "PAP needs to be a number from 0-3, not %d" % alert_pap

        # severity handling
        alert_severity = int(severity) if severity else alert["severity"]

        if alert_severity > 4 or alert_severity < 1:
            return "Severity needs to be a number from 1-4, not %s" % alert_severity

        # tags handling
        if tags:
            if ", " in tags:
                alert_tags = tags.split(", ")
            elif "," in tags:
                alert_tags = tags.split(",")
            else:
                alert_tags = [tags]
        else:
            alert_tags = alert["tags"]     

        # custom fields handling
        alert_customFields = alert["customFields"]

        customfields = CustomFieldHelper()
        if alert_customFields:
            for key, value in alert_customFields.items():
                if list(value)[0] == "integer":
                    customfields.add_integer(key, list(value.items())[0][1])
                elif list(value)[0] == "string":
                    customfields.add_string(key, list(value.items())[0][1])
                elif list(value)[0] == "boolean":
                    customfields.add_boolean(key, list(value.items())[0][1])
                elif list(value)[0] == "float":
                    customfields.add_float(key, list(value.items())[0][1])
                else:
                    print(f'The value type "{value}" of the field {key} is not suported by the function.')

        if isinstance(custom_fields, str):
            try:
                custom_fields = json.loads(custom_fields) if custom_fields else {}
            except json.decoder.JSONDecodeError:
                return "Custom fields need to be valid json"
            
        for key, value in custom_fields.items():
            if type(value) == int:
                customfields.add_integer(key, value)
            elif type(value) == str:
                customfields.add_string(key, value)
            elif type(value) == bool:
                customfields.add_boolean(key, value)
            elif type(value) == float:
                customfields.add_float(key, value)
            else:
                print(f'The value type "{value}" of the field {key} is not suported by the function.')

        alert_customfields = customfields.build()
        
        # Prepare the fields to be updated
        alert = Alert(
            id=id,
            type=alert_type,
            source=alert_source,
            sourceRef=alert_sourceRef,
            title=alert_title,
            description=alert_description,
            tlp=alert_tlp,
            pap=alert_pap,
            severity=alert_severity,
            tags=alert_tags,
            customFields=alert_customfields,
        )
        
        try:
            ret = self.thehive.update_alert(
                alert_id=id,
                alert=alert,
                fields=[
                    "type",
                    "source",
                    "sourceRef",
                    "title",
                    "description",
                    "tlp",
                    "pap",
                    "severity",
                    "tags",
                    "customFields",
                ],
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Update TheHive case Artifact
    def update_case_artifact(
        self,
        url,
        key,
        id,
        description=None,
        tlp=None,
        ioc=None,
        sighted=None,
        tags=None,
        custom_json=None,
    ):
        self.__connect_thehive(url, key)
        # Get Artifact Data
        artifact = self.thehive.get_case_observable(id).json()

        # Prepare fields to be updated
        ## Message (description):
        artifact_message = (
            (
                artifact["message"] + " " + description[1:]
                if "*" == description[0]
                else description
            )
            if description
            else artifact["message"]
        )

        ## TLP, PAP, IOC, Sighted
        artifact_tlp = int(tlp) if tlp else artifact["tlp"]
        artifact_ioc = (
            (False if ioc.lower() == "false" else True) if ioc else artifact["ioc"]
        )
        artifact_sighted = (
            (False if sighted.lower() == "false" else True)
            if sighted
            else artifact["sighted"]
        )

        ## Tags:
        if tags:
            if "*" == tags[0]:
                artifact_tags = tags[1:].split(",")
                artifact_tags.extend(artifact["tags"])
            else:
                artifact_tags = tags.split(",")
        else:
            artifact_tags = artifact["tags"]

        ## Custom Json:
        custom_json = json.loads(custom_json) if custom_json else {}

        artifact = CaseObservable(
            id=id,
            message=artifact_message,
            tlp=artifact_tlp,
            ioc=artifact_ioc,
            sighted=artifact_sighted,
            tags=artifact_tags,
            json=custom_json,
        )

        try:
            ret = self.thehive.update_case_observables(
                artifact, fields=["message", "tlp", "ioc", "sighted", "tags"]
            )
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Create TheHive case Task
    def create_task(
        self,
        url,
        key,
        case_id,
        title,
        description=None,
        status=None,
        flag=None,
        group=None,
        custom_json=None,
    ):
        self.__connect_thehive(url, key)
        # Prepare flag field
        flag = False if flag.lower() == "false" else True
        start_date = (
            round(time.time() * 1000) if status.lower() == "inprogress" else None
        )

        case_task = CaseTask(
            title=title,
            description=description,
            status=status,
            startDate=start_date,
            flag=flag,
            group=group,
            json=custom_json,
        )

        try:
            ret = self.thehive.create_case_task(case_id, case_task)
            ret.raise_for_status()
            return ret.text
        except requests.exceptions.HTTPError as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e.response.text or e)
        except RequestException as e:
            self.logger.error("Error posting to incident response: %s" % e)
            raise Exception(e)

    # Close TheHive case Task
    def update_task(self,url,key,task_id,status):
        if status == "Completed":

            # Add EndDate Time before close
            headers = {
                "Authorization": f"Bearer {apikey}",
            }

            data = {"endDate": round(time.time() * 1000)}
            requests.patch(
                f"{url}/api/case/task/{task_id}",
                headers=headers,
                data=data,
                verify=False,
            )
            task = CaseTask(
                id=task_id,
                status="Completed",
            )
        else:
            task = CaseTask(
            id = task_id,
            status = status,
        )

        response = self.thehive.update_case_task(task, fields=["status"])

        return response.text

if __name__ == "__main__":
    SiemonsterIncidentResponse.run()
