###############################################################
# SIEMonster Core App
# Author: Ilia A. (ilia@siemonster.com, mail@harduino.com)
###############################################################
import requests

from walkoff_app_sdk.app_base import AppBase
from siemonster_core import initSIEMonster

class SiemonsterCore(AppBase):
    __version__ = "1.0.0"
    app_name = "SIEMonster Core"  # this needs to match "name" in api.yaml

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def get_tenants(self, url, key):
        initSIEMonster(self, url, key)
        return self.siemonster_api.get_tenants()    

    def run_global_updates_check(self, url, key):
        initSIEMonster(self, url, key)
        return self.siemonster_api.run_updates_check(self.siemonster_svc.global_tenant_name)

    def run_tenant_updates_check(self, url, key):
        initSIEMonster(self, url, key)
        return self.siemonster_api.run_updates_check(self.siemonster_svc.get_current_tenant_id())
    
    def get_cluster_nodes_info(self, url, key):
        initSIEMonster(self, url, key)
        return self.siemonster_api.get_cluster_nodes_info()

if __name__ == "__main__":
    SiemonsterCore.run()
