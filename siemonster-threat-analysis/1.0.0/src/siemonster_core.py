###############################################################
# SIEMonster Core lib
# Author: Ilia A. (ilia@siemonster.com, mail@harduino.com)
# Version: 1.0.16
###############################################################ч
import requests
import jwt
import uuid
import time
import json
from requests import RequestException

# internal helpers
class SIEMonsterHelpers(object):
    @staticmethod
    def listFromString(str):
        if type(str) == list:
            return str

        str = (
            str.split(", ") if ", " in str else str.split(",") if "," in str else [] if str == "" else [str]
        )
        return str

    @staticmethod
    def unslashifyEnd(url):
        return url[:-1] if url.endswith('/') else url

# work with backend (shuffle api)
class SIEMonsterShuffleApi(object):
    def __init__(self, logger, backend_url):
        self.logger = logger
        self.backend_url = backend_url

    def set_auth(self, auth):
        self.auth = auth

    def execute_get(self, url, params = None, api_version = "v1"):
        url = "{0}/api/{1}{2}".format(
            self.backend_url,
            api_version,
            url,
        )
        response = requests.get(
            url,
            headers=self.get_auth_headers(),
            params=params,
        ).json()

        return response

    def get_user_info(self):
        return self.execute_get("/getinfo")
    
    def get_service_manager_url(self):
        return SIEMonsterHelpers.unslashifyEnd(
            self.execute_get("/siemonster/getservicemanagerurl")["url"],
        )
    
    def get_auth_headers(self):
        if self.auth.apikey is not None:
            return {
                "Authorization": self.auth.bearer_sign + self.auth.apikey,
            }
        else:
            return {}

# work with shuffle auth
class SIEMonsterShuffleAuth(object):
    is_system_user = False
    bearer_sign = "Bearer "
    real_user_email = None
    real_user_info = None
    apikey = None

    def __init__(self, shuffle_api, logger, full_execution, url, key):
        self.shuffle_api = shuffle_api
        self.logger = logger
        self.full_execution = full_execution
        self.url = url
        self.key = key

        self.shuffle_api.set_auth(self)
        self.form_user()

    def set_siemonster_svc(self, siemonster_svc):
        self.siemonster_svc = siemonster_svc

    def form_user(self):
        if 'execution_author' in self.full_execution and self.bearer_sign in self.full_execution['execution_author']:
            self.is_system_user = False
            self.logger.info("acting as a real user")
            self.form_real_user()
        else:
            self.is_system_user = True
            self.logger.info("acting as a system user")

    def form_real_user(self):
        self.apikey = self.full_execution['execution_author'].split(self.bearer_sign, 2)[1]
        self.real_user_info = self.get_user_info()
        self.real_user_email = self.real_user_info["username"]
        
    def get_user_email(self):
        if self.is_system_user:
            return self.get_tenant_system_user_email()
        else:
            return self.real_user_email
    
    def get_tenant_system_user_email(self):
        return f"{self.siemonster_svc.tenant_name}_soar_svc@internal.app"

    def get_user_info(self):
        return self.shuffle_api.get_user_info()

# work with siemonster auth
class SIEMonsterAuth(object):
    jwt_auth_header = "X-Jwt"

    def __init__(self, shuffle_api, shuffle_auth, siemonster_svc, logger, full_execution, url, key):
        self.shuffle_api = shuffle_api
        self.shuffle_auth = shuffle_auth
        self.logger = logger
        self.full_execution = full_execution
        self.url = url
        self.key = key
        self.siemonster_svc = siemonster_svc
        self.siemonster_svc.set_siemonster_auth(self)

    def generate_auth_token(self, additional_payload = None):
        now = int(time.time())
        jwt_headers = {
            'typ': 'JWT',
            'alg': 'HS256',
            'iat': now,
            'exp': now + 1800
        }
        payload = {
            "userName": self.shuffle_auth.get_user_email()
        }

        if additional_payload is not None:
            payload.update(additional_payload)

        token = jwt.encode(payload=payload, key=self.key, headers=jwt_headers)
        return token

    def get_auth_headers(self):
        headers = {}
        headers[self.jwt_auth_header] = self.generate_auth_token()

        return headers

# interact with siemonster service manager service
class SIEMonsterService(object):
    global_tenant_name = "global"
    tenant_name = ""

    def __init__(self, shuffle_api, shuffle_auth, logger, backend_url):
        self.shuffle_api = shuffle_api
        self.shuffle_auth = shuffle_auth
        self.logger = logger
        self.backend_url = backend_url
        self.shuffle_auth.set_siemonster_svc(self)
        self.service_manager_url = self.form_service_manager_url()
        self.form_current_tenant_name()

    def set_siemonster_auth(self, siemonster_auth):
        self.siemonster_auth = siemonster_auth

    def form_service_manager_url(self):
        return self.shuffle_api.get_service_manager_url()

    def form_current_tenant_name(self):
        # FIXME the way should be more "right"
        self.tenant_name = self.backend_url.split("-shuffle-backend")[0].split("http://")[1]

    def get_current_tenant_id(self):
        tenant = self.get_current_tenant()
        if tenant is not None:
            return tenant["id"]
        else:
            return None
        
    def get_payload_key_to_force_session(self, service_name):
        return {
            "forceSession": [
                {
                    "tenantId": self.get_current_tenant_id(),
                    "serviceName": service_name,
                }
            ]
        }

    def get_current_tenant(self):
        tenants = self.siemonster_api.get_tenants()
        for tenant in tenants:
            if tenant["systemName"] == self.tenant_name:
                return tenant
        return None

    def get_auth_gateway_url(self):
        return self.get_global_url("auth-gateway")

    def get_es_url(self):
        return self.get_tenant_url("elasticsearch", "es")

    def get_url(self, service_name, tenant_sign, url_type = "base"):
        response = requests.get(
            url="{0}/api/v1/service/{1}/urlmap/{2}".format(
                self.service_manager_url,
                service_name,
                tenant_sign,
            ),
            headers={
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        ).json()
        result = response['result']['urlMap'][url_type]

        return SIEMonsterHelpers.unslashifyEnd(result)

    def get_tenant_url(self, service_name, url_type = "base"):
        return self.get_url(service_name, self.tenant_name, url_type)
    
    def get_global_url(self, service_name, url_type = "base"):
        return self.get_url(service_name, self.global_tenant_name, url_type)

# work with siemonster's services
# if some API-call used somewhere more than once, it's moved to this class
class SIEMonsterApi(object):
    def __init__(self, logger, shuffle_api, shuffle_auth, siemonster_svc, siemonster_auth):
        self.logger = logger
        self.shuffle_api = shuffle_api
        self.shuffle_auth = shuffle_auth
        self.siemonster_svc = siemonster_svc
        self.siemonster_auth = siemonster_auth

        self.siemonster_svc.siemonster_api = self
        
    def get_events_url(self):
        return "{0}/g/v2/svc/{1}/events/api".format(
            self.siemonster_svc.get_auth_gateway_url(),
            self.siemonster_svc.global_tenant_name,
        )
        
    def get_threat_intel_url(self):
        return "{0}/g/v2/svc/{1}/threat-intel".format(
            self.siemonster_svc.get_auth_gateway_url(),
            self.siemonster_svc.global_tenant_name,
        )

    def get_tenants(self):
        url = "{0}/a/api/v1/rbac/myTenants".format(self.siemonster_svc.get_auth_gateway_url())
        response = requests.get(
            url,
            headers=self.siemonster_auth.get_auth_headers(),
        ).json()

        if "status" not in response or "result" not in response or response['status'] != "success":
            return response

        result = []
        for tenant in response["result"]:
            result.append({
                "id": tenant["id"],
                "shortId": tenant["shortId"],
                "domain": tenant["domain"],
                "systemName": tenant["systemName"],
                "settings": tenant["settings"],
                "settingsReports": tenant["settingsReports"],
                "createdAt": tenant["createdAt"],
                "updatedAt": tenant["updatedAt"],
            })
        return result
    
    def run_updates_check(self, tenant_id):
        url = "{0}/g/v2/svc/{1}/mtapi/api/v2/repo/update".format(self.siemonster_svc.get_auth_gateway_url(), self.siemonster_svc.global_tenant_name)
        return requests.get(
            url,
            headers=self.siemonster_auth.get_auth_headers(),
            params={
                "tenantId": tenant_id,
            }
        ).json()
    
    def get_cluster_nodes_info(self):
        url = "{0}/g/v2/svc/{1}/siemonster/api/v2/admin/cluster/nodes".format(self.siemonster_svc.get_auth_gateway_url(), self.siemonster_svc.global_tenant_name)
        return requests.post(
            url,
            headers=self.siemonster_auth.get_auth_headers(),
        ).json()

class SIEMonsterAlerting(object):
    def __init__(self, logger, shuffle_api, shuffle_auth, siemonster_svc, siemonster_auth, siemonster_api):
        self.logger = logger
        self.shuffle_api = shuffle_api
        self.shuffle_auth = shuffle_auth
        self.siemonster_svc = siemonster_svc
        self.siemonster_auth = siemonster_auth
        self.siemonster_api = siemonster_api

    def get_headers(self):
        headers=self.siemonster_auth.get_auth_headers()
        return {
            **headers,
            'Content-Type': 'application/json',
            'Accept': 'application/json;charset=utf-8',
        }

    def get_alerting_url(self):
        return "{0}/g/v2/svc/{1}/alerting".format(self.siemonster_svc.get_auth_gateway_url(), self.siemonster_svc.global_tenant_name)

    def get_enabled_notifiers(self, type_filter = ""):
        return self.get_notifiers(type_filter, True)

    def get_notifiers(self, type_filter = "", enabled_filter = True):
        url = "{0}/tenant_shipping_methods/{1}".format(
            self.get_alerting_url(),
            self.siemonster_svc.get_current_tenant_id(),
        )
        response = requests.get(
            url,
            headers=self.siemonster_auth.get_auth_headers(),
        ).json()

        if "data" not in response:
            return response
        
        # additional filter if required
        if type_filter != "":
            new_data = []
            types = SIEMonsterHelpers.listFromString(type_filter)
            for notifier in response["data"]:
                if notifier["type"] in types:
                    new_data.append(notifier)
            response["data"] = new_data

        # filter by enabled/disabled state
        if enabled_filter != None:
            new_data = []
            for notifier in response["data"]:
                if (enabled_filter == True and notifier["isActive"] == True) or (enabled_filter == False and notifier["isActive"] == False):
                    new_data.append(notifier)

            response["data"] = new_data

        return response["data"]
    
    def send(self, subject, body, notifiers = "", recipients = ""):
        # form all available notifiers
        available_notifiers = self.get_enabled_notifiers()

        # filter notifiers if required
        if notifiers != "":
            notifiers_list = SIEMonsterHelpers.listFromString(notifiers)
            destinations = []
            for notifier in available_notifiers:
                if notifier["id"] in notifiers_list:
                    destinations.append(notifier)
        else:
            destinations = available_notifiers

        # send to destinations
        for destination in destinations:
            self.alert_to_destination(subject, body, destination, recipients)

        return {"data":"OK"}

    def alert_to_destination(self, subject, body, destination, recipients):
        try:
            payload = {
                "alertId": str(uuid.uuid4()),
                "message": None,
                "type": "soar",
                "useDefailtShippingMethod": False,
                "deliveryNotification": False,
                "shippingMethods": [
                    destination['id']
                ],
                "requestId": str(uuid.uuid4()),
                "tenantName": self.siemonster_svc.tenant_name,
            }

            match destination['type']:
                case 'slack':
                    if subject != "":
                        result_body = f'{subject}\n\n{body}'
                    else:
                        result_body = body
                    payload['message'] = {
                        "body": result_body
                    }

                case 'mailgun' | 'smtp' | 'msgraph':
                    if recipients != "":
                        to = SIEMonsterHelpers.listFromString(recipients)
                    else:
                        to = destination.get('to', None)
                    payload['message'] = {
                        "subject": subject,
                        "body": body,
                        "to": to
                    }

                case 'msteams' | 'pushover' | 'pagerduty':
                    payload['message'] = {
                        "subject": subject,
                        "body": body,
                    }

            if payload['message'] is None:
                raise Exception("unknown destination: %s" % destination['type'])

            response = requests.post(
                f"{self.get_alerting_url()}/alert",
                data=json.dumps(payload),
                headers=self.get_headers()
            )
            response.raise_for_status()
        except RequestException as e:
            self.logger.error("Error posting to siemonster alerter: %s" % e)
    

def initSIEMonster(self, url, key):
    url = SIEMonsterHelpers.unslashifyEnd(url)

    self.shuffle_api = SIEMonsterShuffleApi(
        self.logger,
        self.url,
    )
    self.shuffle_auth = SIEMonsterShuffleAuth(
        self.shuffle_api,
        self.logger,
        self.full_execution,
        url,
        key,
    )
    self.siemonster_svc = SIEMonsterService(
        self.shuffle_api,
        self.shuffle_auth,
        self.logger,
        self.url,
    )
    self.siemonster_auth = SIEMonsterAuth(
        self.shuffle_api,
        self.shuffle_auth,
        self.siemonster_svc,
        self.logger,
        self.full_execution,
        url,
        key,
    )
    self.siemonster_api = SIEMonsterApi(
        self.logger,
        self.shuffle_api,
        self.shuffle_auth,
        self.siemonster_svc,
        self.siemonster_auth,
    )
    self.siemonster_alerting = SIEMonsterAlerting(
        self.logger,
        self.shuffle_api,
        self.shuffle_auth,
        self.siemonster_svc,
        self.siemonster_auth,
        self.siemonster_api,
    )
