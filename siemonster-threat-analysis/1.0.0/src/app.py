#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import urllib3
import cortex4py 
from cortex4py.api import Api

from walkoff_app_sdk.app_base import AppBase
from siemonster_core import initSIEMonster, SIEMonsterHelpers

class SiemonsterThreatAnalysis(AppBase):
    __version__ = "1.0.0"
    app_name = "SIEMonster Threat Analysis"

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        super().__init__(redis, logger, console_logger)

    def __connect_cortex(self, url, key, version=3):
        url = SIEMonsterHelpers.unslashifyEnd(url)
        initSIEMonster(self, url, key)
        
        url = f"{url}/g/v2/svc/{self.siemonster_svc.get_current_tenant_id()}/threat-analysis"
        self.api = Api(
            url,
            self.siemonster_auth.generate_auth_token(
                self.siemonster_svc.get_payload_key_to_force_session('threat-analysis')
            ),
            cert=False,
            version=version,
        )

    def get_available_analyzers(self, url, key, datatype):
        self.__connect_cortex(url, key)

        try:
            analyzers = self.api.analyzers.find_all({}, range='all')
        except cortex4py.exceptions.ServiceUnavailableError as e:
            return [str(e)]
        except cortex4py.exceptions.AuthorizationError as e:
            return [str(e)]
        except cortex4py.exceptions.NotFoundError as e:
            return [str(e)]

        if len(analyzers) == 0:
            return []

        datatype = SIEMonsterHelpers.listFromString(datatype)

        self.logger.info(f"DATATYPE: {datatype} ({len(datatype)} length)")
        self.logger.info(f"cortex api returned {len(analyzers)} analyzers in total")

        all_results = []
        for analyzer in analyzers:
            if len(datatype) == 0 or any(i in datatype for i in analyzer.dataTypeList):
                all_results.append(analyzer.name)

        return all_results

    def run_available_analyzers(self, url, key, data, datatype, message="", tlp=1, force="true"):
        if data == "" or data == "[]":
            return {
                "success": False,
                "reason": "No values to handle []",
            }

        if str(force.lower()) == "true":
            force = 1
        else:
            force = 0

        self.__connect_cortex(url, key)

        analyzers = self.get_available_analyzers(url, key, datatype)

        alljobs = []
        for analyzer in analyzers:
            try:
                job = self.api.analyzers.run_by_name(analyzer, {
                    'data': data,
                    'dataType': datatype,
                    'tlp': tlp,
                    'message': message,
                }, force=force)

                alljobs.append(job.id)
            except cortex4py.exceptions.ServiceUnavailableError as e:
                return [str(e)]
            except cortex4py.exceptions.AuthorizationError as e:
                return [str(e)]
            except cortex4py.exceptions.NotFoundError as e:
                return [str(e)]

        #if len(alljobs) == 1:
        #    return alljobs[0]
        return alljobs

    def run_analyzer(self, url, key, analyzer_name, data, datatype, message="", tlp=1, force="true"):
        if str(force.lower()) == "true":
            force = 1
        else:
            force = 0

        self.__connect_cortex(url, key)

        try:
            job = self.api.analyzers.run_by_name(analyzer_name, {
                'data': data,
                'dataType': datatype,
                'tlp': tlp,
                'message': message,
            }, force=force)
        except cortex4py.exceptions.ServiceUnavailableError as e:
            return str(e)
        except cortex4py.exceptions.AuthorizationError as e:
            return str(e)
        except cortex4py.exceptions.NotFoundError as e:
            return str(e)

        return job.id

    def get_analyzer_result(self, url, key, result_id):
        self.__connect_cortex(url, key)

        try:
            report = self.api.jobs.get_report(result_id).report
        except cortex4py.exceptions.ServiceUnavailableError as e:
            return str(e)
        except cortex4py.exceptions.AuthorizationError as e:
            return str(e)
        except cortex4py.exceptions.NotFoundError as e:
            return str(e)

        return report 

if __name__ == "__main__":
    SiemonsterThreatAnalysis.run()
