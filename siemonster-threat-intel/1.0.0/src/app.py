#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import asyncio
import urllib3
import requests
import base64
import tempfile

from walkoff_app_sdk.app_base import AppBase
from siemonster_core import initSIEMonster

class SiemonsterThreatIntel(AppBase):
    """
    An example of a Walkoff App.
    Inherit from the AppBase class to have Redis, logging, and console logging set up behind the scenes.
    """

    __version__ = "1.0.0"
    app_name = "SIEMonster Threat Analysis"

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        self.verify = False
        super().__init__(redis, logger, console_logger)

    # form required headers to interact with API
    def get_headers(self):
        headers=self.siemonster_auth.get_auth_headers()
        return {
            **headers,
            'Content-Type': 'application/json',
            'Accept': 'application/json;charset=utf-8',
        }

    def simplified_attribute_search(self, url, key, data):
        initSIEMonster(self, url, key)

        url = f"{self.siemonster_api.get_threat_intel_url()}/attributes/restSearch"
        self.logger.debug(f"url: {url}")

        data = {"value": data}
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text

    def simplified_warninglist_search(self, url, key, data):
        initSIEMonster(self, url, key)
        
        url = f"{self.siemonster_api.get_threat_intel_url()}/warninglists/checkValue"
        self.logger.debug(f"url: {url}")

        data = [data]
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text
    
    def simplified_event_search(self, url, key, data):
        initSIEMonster(self, url, key)

        url = f"{self.siemonster_api.get_threat_intel_url()}/events/restSearch"
        self.logger.debug(f"url: {url}")

        data = {"value": data}
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text

    def attributes_search(self, url, key, data):
        initSIEMonster(self, url, key)
        
        url = f"{self.siemonster_api.get_threat_intel_url()}/attributes/restSearch"
        self.logger.debug(f"url: {url}")
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text

    def warninglist_search(self, url, key, data):
        initSIEMonster(self, url, key)
        
        url = f"{self.siemonster_api.get_threat_intel_url()}/warninglists/checkValue"
        self.logger.debug(f"url: {url}")
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text
    
    def events_search(self, url, key, data):
        initSIEMonster(self, url, key)
        
        url = f"{self.siemonster_api.get_threat_intel_url()}/events/restSearch"
        self.logger.debug(f"url: {url}")
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text

    def events_index(self, url, key, data):
        initSIEMonster(self, url, key)
        
        url = f"{self.siemonster_api.get_threat_intel_url()}/events/index"
        self.logger.debug(f"url: {url}")
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text

    def event_edit(self, url, key, event_id, data):
        initSIEMonster(self, url, key)
        
        url = f"{self.siemonster_api.get_threat_intel_url()}/events/edit/{event_id}"
        self.logger.debug(f"url: {url}")
        self.logger.debug(f"data: {data}")

        headers = self.get_headers()

        return requests.post(url, headers=headers, json=data, verify=self.verify).text

    def attributes_downloadsample(self, url, key, md5_list):
        initSIEMonster(self, url, key)
        
        atts_up = []
        items = md5_list if type(md5_list) == list else md5_list.split(",")
        for md5 in items:

            print("Initial md5_list: {}".format(md5_list))

            # value returned from misp is filename|md5
            # the list in converted as string, so items has quote and last closing parenthesis
            md5 = md5.split("|")[-1]
            # .replace("[", "").replace('"', "").replace("]", "")

            print("Downloading with md5: {}".format(md5))

            url = f"{self.siemonster_api.get_threat_intel_url()}/attributes/downloadSample/{md5}"
            self.logger.debug(f"url: {url}")
            misp_headers = self.get_headers()

            # get sample by md5 (works only if md5 is related to file object)
            ret = requests.get(url, headers=misp_headers, verify=self.verify)

            if ret.status_code == 200:
                sample = ret.json().get("result", None)
                if sample and len(sample) == 1:
                    sample = sample[0]
                elif not sample and ret.json().get("message", None):
                    return "Return message: {}".format(ret.json()["message"])

                atts_up.append(
                    {
                        "filename": "{}.zip".format(sample["filename"]),
                        "data": base64.b64decode(sample["base64"]),
                    }
                )
            else:
                return "Issue downloading {}".format(md5)

        if len(atts_up) > 0:
            uuids = self.set_files(atts_up)
            return uuids


if __name__ == "__main__":
    SiemonsterThreatIntel.run()
