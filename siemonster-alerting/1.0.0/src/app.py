###############################################################
# SIEMonster Alerting App
# Author: Ilia A. (ilia@siemonster.com, mail@harduino.com)
###############################################################
import requests

from walkoff_app_sdk.app_base import AppBase
from siemonster_core import initSIEMonster

class SiemonsterAlerting(AppBase):
    __version__ = "1.0.0"
    app_name = "SIEMonster Alerting"  # this needs to match "name" in api.yaml

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def get_notifiers(self, url, key, type_filter):
        initSIEMonster(self, url, key)
        return self.siemonster_alerting.get_enabled_notifiers(type_filter)
    
    def send(self, url, key, subject, body, notifiers, recipients):
        initSIEMonster(self, url, key)
        return self.siemonster_alerting.send(subject, body, notifiers, recipients)

if __name__ == "__main__":
    SiemonsterAlerting.run()
